# OpenEMA API

Install the required python dependencies with

```python
pip install -r requirements.txt
```

And start a development version of the API using

```python
uvicorn api:app --reload
```

You can then browse to [http://localhost:8000/docs](http://localhost:8000/docs) to see the API in action 