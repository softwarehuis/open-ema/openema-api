create schema if not exists analyses;
drop table if exists analyses.icsr_temp;

create table analyses.icsr_temp as
    select local_number, max(report_date) as report_date, max(country) as country from import.substance group by local_number;
insert into analyses.icsr_temp
    select local_number, max(report_date) as report_date, max(country) as country from import.product group by local_number;

drop table if exists analyses.icsr;

CREATE TABLE analyses.icsr AS
  SELECT
    local_number,
    max(report_date) AS report_date,
    max(country) as country
  FROM analyses.icsr_temp
  GROUP BY local_number;

drop table if exists analyses.icsr_temp;

drop table if exists analyses.reports_per_year;
drop table if exists analyses.temp_1;
drop table if exists analyses.temp_2;
create table analyses.temp_1 as
SELECT
    extract(year from date_trunc('year', report_date)) AS "year",
    count(*) as "all"
FROM analyses.icsr
GROUP BY date_trunc('year', report_date);

create table analyses.temp_2 as select A."year", "all", "eea", "neea" from analyses.temp_1 as A
left join
( SELECT B.year, "eea", "neea" from
(SELECT
    extract(year from date_trunc('year', report_date)) AS "year",
    count(*) as "eea"
FROM analyses.icsr
where country='European Economic Area'
GROUP BY date_trunc('year', report_date)) B
LEFT JOIN
(SELECT
    extract(year from date_trunc('year', report_date)) AS "year",
    count(*) as "neea"
FROM analyses.icsr
where country='Non European Economic Area'
GROUP BY date_trunc('year', report_date)) C
on B.year = C.year) D on A.year = D.year;

create table analyses.reports_per_year as
select A."year", "all", "eea", "neea", "other" from analyses.temp_2 as A
left join
(SELECT
    extract(year from date_trunc('year', report_date)) AS "year",
    count(*) as "other"
FROM analyses.icsr
where country='Not Specified'
GROUP BY date_trunc('year', report_date)) B
on A.year = B.year;
drop table if exists analyses.temp_1;
drop table if exists analyses.temp_2;

drop table if exists analyses.deaths;
create table analyses.deaths as
select
    seriousness_criteria,
    count(distinct local_number),
    array_agg(distinct product_id)
from jun_2023.product_reaction where seriousness_criteria ilike '%death%' group by seriousness_criteria;

drop table if exists analyses.death_by_product;
create table analyses.death_by_product as
select
    product_id,
    count(distinct local_number)
from jun_2023.product_reaction where seriousness_criteria ilike '%death%' group by product_id;

drop table if exists analyses.deaths_per_product;
create table analyses.deaths_per_product as select
    product_id,
    count(distinct local_number)
from jun_2023.product_reaction where seriousness_criteria ilike '%death%' group by product_id order by count(distinct local_number) desc;