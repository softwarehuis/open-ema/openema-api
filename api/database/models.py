from sqlalchemy import Column, Integer, String, DateTime, Boolean
from .database import Base
from ..settings import Settings


class Substance(Base):
    __tablename__ = "substances"
    __table_args__ = {"schema": Settings.DB_SCHEMA}
    substance_id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)


class SubstanceReport(Base):
    __tablename__ = "substance"
    __table_args__ = {"schema": Settings.DB_SCHEMA}
    substance_id = Column(Integer, index=True, primary_key=True)
    local_number = Column(String, index=True, primary_key=True)
    report_type = Column(String, index=True, primary_key=True)
    report_date = Column(DateTime)
    qualification = Column(String, index=True)
    country = Column(String, index=True)
    age_group_lower_months = Column(Integer)
    age_group_upper_months = Column(Integer)
    age_group_reporter = Column(String, index=True)
    is_child_report = Column(Boolean)
    sex = Column(String)
    serious = Column(Boolean)


class Product(Base):
    __tablename__ = "products"
    __table_args__ = {"schema": Settings.DB_SCHEMA}
    product_id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)


class ProductReport(Base):
    __tablename__ = "product"
    __table_args__ = {"schema": Settings.DB_SCHEMA}
    product_id = Column(Integer, index=True, primary_key=True)
    local_number = Column(String, index=True, primary_key=True)
    report_type = Column(String, index=True)
    report_date = Column(DateTime, primary_key=True)
    qualification = Column(String, index=True)
    country = Column(String, index=True)
    age_group_lower_months = Column(Integer)
    age_group_upper_months = Column(Integer)
    age_group_reporter = Column(String, index=True)
    is_child_report = Column(Boolean)
    sex = Column(String)
    serious = Column(Boolean)


class ReportsPerYear(Base):
    __tablename__ = "reports_per_year"
    __table_args__ = {"schema": "analyses"}
    year = Column(Integer, index=True, primary_key=True)
    all = Column(Integer, index=True)
    eea = Column(Integer, index=True)
    neea = Column(Integer, index=True)
    other = Column(Integer, index=True)
