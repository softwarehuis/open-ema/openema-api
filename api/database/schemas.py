from datetime import datetime
from typing import Optional

from pydantic import BaseModel


class ItemBase(BaseModel):
    name: str


class ReportBase(BaseModel):
    local_number: str
    report_type: str
    report_date: datetime
    qualification: str
    country: str
    age_group_lower_months: Optional[int]
    age_group_upper_months: Optional[int]
    age_group_reporter: Optional[str]
    is_child_report: bool
    sex: Optional[str]
    serious: Optional[bool]


class Substance(ItemBase):
    substance_id: int

    class Config:
        from_attributes = True


class SubstanceReport(ReportBase):
    substance_id: int

    class Config:
        from_attributes = True


class Product(ItemBase):
    product_id: int

    class Config:
        from_attributes = True


class ProductReport(ReportBase):
    product_id: int

    class Config:
        from_attributes = True
