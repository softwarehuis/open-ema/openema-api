from sqlalchemy import func, distinct
from sqlalchemy.orm import Session

from ..database import models


def get_adverse_events_count(db: Session):
    _unique_product_count = db.query(
        func.count(distinct(models.ProductReport.local_number))
    ).first()

