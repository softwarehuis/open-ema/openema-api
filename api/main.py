from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from .substance import router as substance_router
from .product import router as product_router
from .report import router as report_router

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(product_router)
app.include_router(substance_router)
app.include_router(report_router)
