from sqlalchemy import func
from sqlalchemy.orm import Session

from ..database import models


def db_line_listings_for_local_number(db: Session, key: str, skip: int = 0, limit: int = 100):
    product_reports = db.query(
        models.ProductReport).where(models.ProductReport.local_number == key).offset(skip).limit(limit).all()
    substance_reports = db.query(
        models.SubstanceReport).where(models.SubstanceReport.local_number == key).offset(skip).limit(limit).all()
    return [] + product_reports + substance_reports


def db_unique_reports(db: Session, skip: int = 0, limit: int = 0):
    product_local_numbers = [r.local_number for r in
                             db.query(models.ProductReport.local_number).offset(skip).limit(limit).all()]
    substance_local_numbers = [r.local_number for r in
                               db.query(models.SubstanceReport.local_number).offset(skip).limit(limit).all()]
    _total = list(set(product_local_numbers + substance_local_numbers))
    _total.sort()
    return _total


def db_reports_total(db: Session):
    _result = [{"all": r[0], "eea": r[1], "neea": r[2], "other": r[3]} for r in db.query(
        func.sum(models.ReportsPerYear.all),
        func.sum(models.ReportsPerYear.eea),
        func.sum(models.ReportsPerYear.neea),
        func.sum(models.ReportsPerYear.other)
    ).all()]
    return _result[0]


def db_reports_year(db: Session):
    _result = db.query(models.ReportsPerYear).all()
    return _result
