from typing import Union

from fastapi import Depends, APIRouter
from sqlalchemy.orm import Session

from ..database import models, schemas
from ..database.database import SessionLocal, engine
from .functions import db_line_listings_for_local_number, db_unique_reports, db_reports_total, db_reports_year

models.Base.metadata.create_all(bind=engine)

router = APIRouter(tags=["Reports"])


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.get("/reports",  # response_model=list[str]
)
def get_reports(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    reports = db_unique_reports(db, skip=skip, limit=limit)
    return reports


@router.get("/reports/yearly",  # response_model=list[str]
)
def get_reports_yearly(db: Session = Depends(get_db)):
    reports = db_reports_year(db)
    return reports


@router.get("/reports/total",  # response_model=list[str]
)
def get_reports(db: Session = Depends(get_db)):
    reports = db_reports_total(db)
    return reports


@router.get("/report/{local_number}", response_model=list[Union[schemas.ProductReport, schemas.SubstanceReport]])
def get_line_listings_for_local_number(local_number: str, skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    reports = db_line_listings_for_local_number(db, key=local_number, skip=skip, limit=limit)
    return reports
