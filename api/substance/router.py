from fastapi import Depends, APIRouter, HTTPException
from sqlalchemy.orm import Session

from ..database import models, schemas
from ..database.database import SessionLocal, engine
from .functions import get_substance, get_substances, get_substance_by_name, get_statistics, get_substance_statistics, \
    get_substance_reports

models.Base.metadata.create_all(bind=engine)

router = APIRouter(tags=["Substances"])


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.get("/substances/", response_model=list[schemas.Substance])
def get_substance_list(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    substances = get_substances(db, skip=skip, limit=limit)
    return substances


@router.get("/substance/search/{term}", response_model=list[schemas.Substance])
def search_substance(term: str, db: Session = Depends(get_db)):
    db_substance = get_substance_by_name(db, value=term)
    if db_substance is None:
        raise HTTPException(status_code=404, detail=f"No substances found for {term}")
    return db_substance


@router.get(
    "/substance/{substance_id}/reports",
    response_model=list[schemas.SubstanceReport],
    response_model_exclude_none=True,
    response_model_exclude_unset=True
)
def get_substance__reports_by_id(substance_id: int, skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    reports = get_substance_reports(db, key=substance_id, skip=skip, limit=limit)
    if reports is None:
        raise HTTPException(status_code=404, detail="substance not found")
    return reports


@router.get(
    "/substance/statistics"
)
def get_all_statistics_for_substance(db: Session = Depends(get_db)):
    reports = get_statistics(db)
    if reports is None:
        raise HTTPException(status_code=404, detail="statistics for substances not found")
    return reports


@router.get(
    "/substance/{substance_id}/statistics"
)
def get_statistics_for_substance(substance_id: int, db: Session = Depends(get_db)):
    reports = get_substance_statistics(db, key=substance_id)
    if reports is None:
        raise HTTPException(status_code=404, detail="substance not found")
    return reports


@router.get("/substance/{substance_id}", response_model=schemas.Substance)
def get_substance_by_id(substance_id: int, db: Session = Depends(get_db)):
    db_substance = get_substance(db, key=substance_id)
    if db_substance is None:
        raise HTTPException(status_code=404, detail="Substance not found")
    return db_substance
