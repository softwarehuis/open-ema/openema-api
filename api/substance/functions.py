from sqlalchemy import func
from sqlalchemy.orm import Session

from ..database import models


def get_substance(db: Session, key: int):
    return db.query(models.Substance).filter(models.Substance.substance_id == key).first()


def get_substance_by_name(db: Session, value: str):
    search = "%{}%".format(value)
    return db.query(models.Substance).filter(models.Substance.name.ilike(search)).all()


def get_substances(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Substance).offset(skip).limit(limit).all()


def get_substance_reports(db: Session, key: int, skip: int = 0, limit: int = 100):
    return db.query(models.SubstanceReport).where(models.SubstanceReport.substance_id == key).offset(skip).limit(limit).all()


def get_substance_statistics(db: Session, key: int):
    number_of_reports, min_date, max_date = db.query(
        func.count(models.SubstanceReport.local_number),
        func.min(models.SubstanceReport.report_date),
        func.max(models.SubstanceReport.report_date)
    ).where(models.SubstanceReport.substance_id == key).first()
    return {"reports": number_of_reports, "first": min_date, "last": max_date}


def get_statistics(db: Session):
    number_of_reports, min_date, max_date = db.query(
        func.count(models.SubstanceReport.local_number),
        func.min(models.SubstanceReport.report_date),
        func.max(models.SubstanceReport.report_date)
    ).first()
    return {"reports": number_of_reports, "first": min_date, "last": max_date}

