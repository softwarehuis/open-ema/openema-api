from fastapi import Depends, APIRouter, HTTPException
from sqlalchemy.orm import Session

from ..database import models, schemas
from ..database.database import SessionLocal, engine
from .functions import get_product, get_products, get_product_by_name, get_product_reports, get_product_statistics, \
    get_statistics

models.Base.metadata.create_all(bind=engine)

router = APIRouter(tags=["Products"])


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.get("/products/", response_model=list[schemas.Product])
def get_product_list(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    products = get_products(db, skip=skip, limit=limit)
    return products


@router.get("/product/search/{term}", response_model=list[schemas.Product])
def search_product(term: str, db: Session = Depends(get_db)):
    db_product = get_product_by_name(db, value=term)
    if db_product is None:
        raise HTTPException(status_code=404, detail=f"No products found for {term}")
    return db_product


@router.get(
    "/product/{product_id}/reports",
    response_model=list[schemas.ProductReport],
    response_model_exclude_none=True,
    response_model_exclude_unset=True
)
def get_product_reports_by_id(product_id: int, skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    reports = get_product_reports(db, key=product_id, skip=skip, limit=limit)
    if reports is None:
        raise HTTPException(status_code=404, detail="product not found")
    return reports


@router.get(
    "/product/statistics"
)
def get_all_statistics_for_products(db: Session = Depends(get_db)):
    reports = get_statistics(db)
    if reports is None:
        raise HTTPException(status_code=404, detail="statistics for products not found")
    return reports


@router.get(
    "/product/{product_id}/statistics"
)
def get_statistics_for_product(product_id: int, db: Session = Depends(get_db)):
    reports = get_product_statistics(db, key=product_id)
    if reports is None:
        raise HTTPException(status_code=404, detail=f"statistics for {product_id} not found")
    return reports


@router.get("/product/{product_id}", response_model=schemas.Product)
def get_product_by_id(product_id: int, db: Session = Depends(get_db)):
    db_product = get_product(db, key=product_id)
    if db_product is None:
        raise HTTPException(status_code=404, detail="product not found")
    return db_product
