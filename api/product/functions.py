from sqlalchemy import func
from sqlalchemy.orm import Session

from ..database import models


def get_product(db: Session, key: int):
    return db.query(models.Product).filter(models.Product.product_id == key).first()


def get_product_by_name(db: Session, value: str):
    search = "%{}%".format(value)
    return db.query(models.Product).filter(models.Product.name.ilike(search)).all()


def get_products(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Product).offset(skip).limit(limit).all()


def get_product_reports(db: Session, key: int, skip: int = 0, limit: int = 100):
    return db.query(models.ProductReport).where(models.ProductReport.product_id == key).offset(skip).limit(limit).all()


def get_product_statistics(db: Session, key: int):
    number_of_reports, min_date, max_date = db.query(
        func.count(models.ProductReport.local_number),
        func.min(models.ProductReport.report_date),
        func.max(models.ProductReport.report_date)
    ).where(models.ProductReport.product_id == key).first()
    return {"reports": number_of_reports, "first": min_date, "last": max_date}


def get_statistics(db: Session):
    number_of_reports, min_date, max_date = db.query(
        func.count(models.ProductReport.local_number),
        func.min(models.ProductReport.report_date),
        func.max(models.ProductReport.report_date)
    ).first()
    return {"reports": number_of_reports, "first": min_date, "last": max_date}
